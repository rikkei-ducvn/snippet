<?php

/**
 * Class Model_Analytics_Search_Console
 */
class Model_Analytics_Search_Console extends Model_Base
{
    /**
     * Search consoles
     * @return array
     */
    public static function search_consoles()
    {
        $query = DB::select('*')
            ->from('analytics_search_console');

        return $query
            ->order_by('id', 'DESC')
            ->limit(100)
            ->execute()
            ->as_array();
    }

    /**
     * Get detail
     * @param int $id
     * @return array
     */
    public static function get_detail($id)
    {
        return DB::select('*')
            ->from('analytics_search_console')
            ->where('id', '=', $id)
            ->execute()
            ->current();
    }

    /**
     * Update console
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public static function update($id, $data)
    {
        return DB::update('analytics_search_console')
            ->set($data)
            ->where('id', $id)
            ->execute();
    }

    /**
     * Insert console
     * @param string $data
     * @return mixed
     */
    public static function insert($params, $data_bind)
    {
        $sql = <<<SQL
            INSERT INTO `analytics_search_console` (`date`, `keyword`, `click_number`, `display_times`, `ctr`, `posting_rank`, `created_at`) 
            VALUES  $params
SQL;


        $query = DB::query($sql);
        $query->parameters($data_bind);
        return $query->execute();
    }
}

// Define Constant

var CONFIRM_DELETE = 'このデータを削除します。よろしいですか？';
var CONFIRM_DELETE_EXCLASSROOM = '課外契約種目「契約No{code}」を削除します。よろしいですか？';
var CONFIRM_DELETE_DISCOUNT = '複合割引リスト「複合No{code}」を削除します。よろしいですか？';
var CONFIRM_DELETE_GARDEN = 'この園データを削除します。よろしいですか？';
var CONFIRM_DELETE_DISCOUNT_DETAIL = '明細「連番{number_serial}」を削除します。よろしいですか？';
var CONFIRM_DELETE_TIMETABLE = '時間割コード「{code}」を削除します。よろしいですか？';
var CONFIRM_DELETE_CUSTOMER_TEL = '追加連絡先情報「{id_customer_tel}」を削除します。よろしいですか？';
var CONFIRM_SEARCH_CUSTOMER_PDF = '印刷画面を表示します。よろしいですか？';
var BANK_REQUIRED = '銀行を入力してください。';
var MSG_NO_DATA = 'データを取得できません。';
var ARY_SCHOOL_YEAR = {
    '~2': '未就園',
    '3': '年少',
    '4': '年中',
    '5': '年長',
    '6': '小学1年',
    '7': '小学2年',
    '8': '小学3年',
    '9': '小学4年',
    '10': '小学5年',
    '11': '小学6年',
    '12': '中学1年',
    '13': '中学2年',
    '14': '中学3年',
    '15': '高校1年',
    '16': '高校2年',
    '17': '高校3年',
    '18~': 'その他'
};

/**
 * Show error
 *
 * @param {string} msg
 */
function system_error(msg) {
    $('#msgError').html(msg);
    $('#dialog_system_error').modal('show');
}

/**
 * Template Replacement
 *
 * @param {string} template
 * @param {object} data
 * @returns {string}
 */
function template(template, data) {
    return template.replace(
        /{(\w*)}/g,
        function (m, key) {
            return data.hasOwnProperty(key) ? data[key] : '';
        }
    );
}

/**
 * Set zindex to modal
 */
function set_zindex_to_modal() {
    var zIndex = 1050;
    var modal = $('.modal.in');
    var backdrop = $('.modal-backdrop');
    $(backdrop).each(function (index, element) {
        $(element).css('z-index', zIndex + 20 * index);
        $(modal[index]).css('z-index', zIndex + 20 * index + 10);
    });
}

/**
 * Number format
 * @param {string} number
 * @param {string} err
 * @return {string}
 */
function number_format(number, err) {
    err = err || 0;

    if (isNaN(number)) {
        return err;
    }

    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * Convert string to number
 * @param {string} str
 * @return {number}
 */
function convert_number(str) {
    return parseInt(str.toString().replace(/,/g, ""));
}

/**
 * Minus
 * @param {number} first
 * @param {number} two
 * @param {number} err
 * @return {*}
 */
function minus(first, two, err) {
    err = err || 0;

    if (isNaN(first) || isNaN(two) || first === '' || two === '') {
        return err;
    }

    return parseFloat(first) - parseFloat(two);
}

/**
 * Get school year
 * @param {number} year
 * @param {number} month
 * @returns {string}
 */
function get_school_year(year, month) {
    var today = new Date();
    var today_month = today.getMonth() + 1;
    var age = today.getFullYear() - year;
    var school_year = ARY_SCHOOL_YEAR['~2'];

    if ((today_month == 1 || today_month == 2 || today_month == 3) && month > 3) {
        age--;
    }

    if (ARY_SCHOOL_YEAR[age] != undefined) {
        school_year = ARY_SCHOOL_YEAR[age];
    } else if (age > 17) {
        school_year = ARY_SCHOOL_YEAR['18~'];
    }

    return school_year;
}

$(function () {
    var topBtn = $('#pageTop');
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 80) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    /**
     * モーダルダイアログの重なりを調整
     **/
    $(document).on('click', '[data-toggle="modal"]', function () {
        window.setTimeout(set_zindex_to_modal, 0);
    });

    $('.sortable').sortable();

    // Date picker
    $('.datepicker').datepicker({
        // Set the first day of the week: Sunday is 0, Monday is 1, etc.
        firstDay: 1
    });

    // Redirect back from page confirm
    $('.btn-back').click(function () {
        window.history.back();
    });
});

<?php

/**
 * Class Get_Order
 * Get information of number in number sequence
 */
class Get_Order
{
    /**
     * Get number value by order
     * @access  public
     * @param int $order
     * @return int
     */
    public function get_order_value($order)
    {
        return ceil($order / 2) + 1;
    }
}

Open git in C:\xampp\htdocs clone all source. Then we have htdocs: C:\xampp\htdocs\snippet

To test turn on XAMPP and goto:	

	+ Url Snipet1: htp://localhost/snippet/snippet1
	+ Url Snipet2: htp://localhost/snippet/snippet2
	+ Url Snipet3: htp://localhost/snippet/snippet3

For snippet2:
 + Create a DB name: snippet2
 + Open cmd in C:\xampp\htdocs\snippet\snippet2 and run:
 
 + php oil refine migrate
 + php oil refine migrate:current
 
 To review code:
 
 + Snippet1:
 + C:\xampp\htdocs\snippet\snippet1\get_order.php
 + C:\xampp\htdocs\snippet\snippet1\index.php
 
 - Snippet2:
 + C:\xampp\htdocs\snippet\snippet2\fuel\app\classes\controller\analytics\search\console.php
 + C:\xampp\htdocs\snippet\snippet2\fuel\app\classes\business\analytics\search\console.php
 + C:\xampp\htdocs\snippet\snippet2\fuel\app\classes\model\analytics\search\console.php
 + C:\xampp\htdocs\snippet\snippet2\fuel\app\views\analytics\search\index.php
 + C:\xampp\htdocs\snippet\snippet2\public\assets\js\index.js
 
 - Snippet3:
 + C:\xampp\htdocs\snippet\snippet3\index.php
 + C:\xampp\htdocs\snippet\snippet3\asset\js\index.js
 


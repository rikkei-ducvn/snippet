<div class="container">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Manage <b>Analytics Search Console</b></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <form action="/snippet/snippet2/analytics/search/console/insert" method="post">
                        <h4>Enter number large random data your want insert:</h4>
                        <div class="form-group">
                            <input name="number_record" id="number_record" type="text" class="form-control"
                                   style="width:50%;">
                        </div>
                        <button type="submit" id="insert" class="btn btn-success"><i class="material-icons">&#xE147;</i>
                            <span>INSERT</span>
                        </button>

                    </form>
                </div>
            </div>
        </div>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Keyword</th>
                <th>Click number</th>
                <th>Display times</th>
                <th>Ctr</th>
                <th>Posting rank</th>
                <th>Created at</th>
            </tr>
            </thead>
            <tbody>
            <?php if (empty($consoles)): ?>
                <tr>
                    <td colspan="6" class="text-center"><?php echo 'DB empty please insert Data' ?></td>
                </tr>
            <?php endif; ?>
            <?php foreach ($consoles as $console): ?>
                <tr>
                    <td><?php echo $console['id'] ?></td>
                    <td><?php echo $console['date'] ?></td>
                    <td><?php echo $console['keyword'] ?></td>
                    <td><?php echo $console['click_number'] ?></td>
                    <td><?php echo $console['display_times'] ?></td>
                    <td><?php echo $console['ctr'] ?></td>
                    <td><?php echo $console['posting_rank'] ?></td>
                    <td><?php echo date('Y/m/d H:i:s', $console['created_at']) ?></td>
                    <td>
                        <button data-id="<?php echo $console['id'] ?>" class="edit" data-toggle="modal"><i
                                    class="material-icons"
                                    data-toggle="tooltip"
                                    title="Edit">&#xE254;</i></button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- Edit Modal HTML -->
<div id="editEmployeeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-update">
                <div class="modal-header">
                    <h4 class="modal-title">Edit console</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Date</label>
                        <input name="date" id="date" type="text" class="form-control">
                        <input name="id" id="id" type="hidden">
                        <input name="date_check" id="date_check" type="hidden">
                    </div>
                    <div class="form-group">
                        <label>Keyword</label>
                        <input name="keyword" id="keyword" type="text" class="form-control">
                        <input name="keyword_check" id="keyword_check" type="hidden">
                    </div>
                    <div class="form-group">
                        <label>Click number</label>
                        <input name="click_number" id="click_number" type="text" class="form-control">
                        <input name="click_number_check" id="click_number_check" type="hidden">
                    </div>
                    <div class="form-group">
                        <label>Display times</label>
                        <input name="display_times" id="display_times" type="text" class="form-control">
                        <input name="display_times_check" id="display_times_check" type="hidden">
                    </div>
                    <div class="form-group">
                        <label>Ctr</label>
                        <input name="ctr" id="ctr" type="text" class="form-control">
                        <input name="ctr_check" id="ctr_check" type="hidden">
                    </div>
                    <div class="form-group">
                        <label>Posting rank</label>
                        <input name="posting_rank" id="posting_rank" type="text" class="form-control">
                        <input name="posting_rank_check" id="posting_rank_check" type="hidden">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

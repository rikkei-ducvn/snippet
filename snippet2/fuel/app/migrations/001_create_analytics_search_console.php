<?php

namespace Fuel\Migrations;

class Create_analytics_search_console
{
	public function up()
	{
		\DBUtil::create_table('analytics_search_console', array(
			'id' => array('type' => 'int', 'unsigned' => true, 'null' => false, 'auto_increment' => true, 'constraint' => '11'),
			'date' => array('null' => false, 'type' => 'text'),
			'keyword' => array('constraint' => 255, 'null' => false, 'type' => 'varchar'),
			'click_number' => array('constraint' => '11', 'null' => false, 'type' => 'int'),
			'display_times' => array('constraint' => '11', 'null' => false, 'type' => 'int'),
			'ctr' => array('constraint' => '11', 'null' => true, 'type' => 'int', 'unsigned' => true),
			'posting_rank' => array('constraint' => '11', 'null' => true, 'type' => 'int', 'unsigned' => true),
			'created_at' => array('constraint' => '11', 'null' => true, 'type' => 'int', 'unsigned' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('analytics_search_console');
	}
}
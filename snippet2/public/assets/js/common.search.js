var Setting = {
    /**
     * Apply
     * @param {object} el
     * @return {Setting}
     */
    apply: function (el) {
        var bind = $(el).attr('bind-setting');

        if ($(el).is(':checked')) {
            $('[data-setting="' + bind + '"]').show();
        } else {
            $('[data-setting="' + bind + '"]').hide();
        }

        return this;
    },

    /**
     * Default show/hide setting column
     * @return {Setting}
     */
    default: function () {
        var instance = this;

        $('#dialog_setting input').each(function () {
            instance.apply(this);
        });

        return this;
    },

    /**
     * Init
     * @return {Setting}
     */
    init: function () {
        var instance = this;

        // Event when change value on setting dialog
        $(document).on('change', '#dialog_setting input', function () {
            instance.apply(this);
        });

        return this;
    }
};

var Search = {
    /**
     * Variable content StaticSearch object create from library
     */
    static_search: null,
    autoload: false,

    /**
     * Init search object
     * @return {Search}
     */
    init: function () {
        $('#dialog_search').modal('show');
        $('#datalist').hide();

        this.static_search = new StaticSearch({
            autoload: this.autoload,
            dataType: 'JSON',
            cache: false,
            methods: {
                render: {
                    success: function (rs) {
                        if (rs.err) {
                            system_error(rs.msg);
                        } else {
                            $('.ss-success').html(rs.html);
                        }
                    },
                    error: function (rs) {
                        system_error(rs.statusText);
                    }
                }
            },
            events: {
                submit: function () {
                    $('#dialog_search').modal('hide');
                    $('#datalist').show();
                },
                rendered: function (type, content) {
                    Setting.default();
                }
            }
        });

        this.static_search.run();
        return this;
    },

    /**
     * Reload data after change
     * @return {Search}
     */
    reload: function () {
        this.static_search.reload();
        return this;
    }
};
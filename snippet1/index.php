<html>
<head>
    <head>
        <title>Snippet 1</title>
        <meta charset="utf-8">
        <meta content="text/html; charset=utf-8"/>
        <link type="text/css" rel="stylesheet" href="asset/css/bootstrap.css"/>
    </head>
</head>
<body>
<div class="container text-center">
    <form action="index.php" method="get">
        <table class="table table-bordered" style="width: auto; margin: 60 auto" ;>
            <tr>
                <td>Please input order :</td>
                <td><input type="text" name="order" size="25"/> <br/></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <button type="submit" class="btn btn-primary">Get result</button>
                </td>
            </tr>
        </table>
    </form>
    <?php
    require('get_order.php');

    if (isset($_GET['order'])) {
        if (is_numeric($_GET['order'])) {
            $order = $_GET['order'];
            $result = Get_Order::get_order_value($order);

            echo 'The Result is: ' . $result;
        } else {
            echo 'Please check input order';
        }
    }
    ?>
</div>
</body>
</html>

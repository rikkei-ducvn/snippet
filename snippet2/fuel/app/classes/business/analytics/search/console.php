<?php

/**
 * Class Business_Analytics_Search_Console
 */
class Business_Analytics_Search_Console
{
    /**
     * Search consoles
     * @return array
     */
    public function search_consoles()
    {
        $consoles = Model_Analytics_Search_Console::search_consoles();

        return $consoles;
    }

    /**
     * Get detail of console by id
     * @param int $id
     * @return array
     */
    public function get_detail($id)
    {
        return Model_Analytics_Search_Console::get_detail($id);
    }

    /**
     * Insert console
     * @return array 0: the last id inserted, 1: affected rows
     */
    public function insert_console()
    {
        $number_record = Input::post('number_record');
        $number = 1000;
        if (is_numeric($number_record)) {
            // Insert random data
            while ($number_record > $number) {
                $this->insert_console_segment($number);
                $number_record = $number_record - $number;
            }

            $this->insert_console_segment($number_record);
        }
    }

    /**
     * Insert console segment
     * @param int $number
     * @return void
     */
    private function insert_console_segment($number)
    {
        $data_bind = array();
        $params = array();

        // Create random data
        for ($i = 1; $i <= $number; $i++) {
            $data_bind = array_merge($data_bind, array(
                'date' . $i => date('Ym'),
                'keyword' . $i => 'keyword' . $i,
                'click_number' . $i => $i,
                'display_times' . $i => $i,
                'ctr' . $i => $i,
                'posting_rank' . $i => $i,
                'created_at' . $i => strtotime(date("Y-m-d H:i:s")),
            ));

            array_push($params, array(
                ':date' . $i,
                ':keyword' . $i,
                ':click_number' . $i,
                ':display_times' . $i,
                ':ctr' . $i,
                ':posting_rank' . $i,
                ':created_at' . $i
            ));
        }

        foreach ($params as $key => $value) {
            $params[$key] = implode(" ,", $params[$key]);
        }

        $params = implode("), (", $params);
        $params = '(' . $params . ')';

        Model_Analytics_Search_Console::insert($params, $data_bind);
    }

    /**
     * Update console
     * @param int $id
     * @return mixed
     */
    public function update_console($id)
    {
        $data = array(
            'date' => Input::post('date'),
            'keyword' => Input::post('keyword'),
            'click_number' => Input::post('click_number'),
            'display_times' => Input::post('display_times'),
            'ctr' => Input::post('ctr'),
            'posting_rank' => Input::post('posting_rank'),
        );

        return Model_Analytics_Search_Console::update($id, $data);
    }
}

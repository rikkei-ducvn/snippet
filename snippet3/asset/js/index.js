$(function () {
    var id = 0;
    var sum = 0;

    // Multiple images preview in browser
    var imagesPreview = function (input, placeToInsertImagePreview) {
        if (input.files) {
            var reader = new FileReader();

            // Add image preview
            reader.onload = function (event) {
                $('<div class="thumbnail FqEXhcfmBAIO7XHZs22Da" style="background-image: url(' + event.target.result + '"><button type="button" data-id="' + id + '" class="btn btn-close btn-xs"><i class="fa fa-times"></i></button></div>')
                    .insertBefore('._3wLdyQ1JXeAenU3yteGpQI');
            }

            id++;
            sum++;

            // Add one more input upload file
            $('<input type="file" name="gallery-photo-add[]" id="gallery-photo-add' + id + '" style="visibility: hidden; height: 0;"/>').insertAfter('#gallery-photo-add' + (id - 1));
            reader.readAsDataURL(input.files[0]);

            if (sum > 2) {
                $('#alert').css('display', 'none');
            }
        }
    };

    // Event when close image
    $(document).on('click', '.btn-xs', function () {
        $(this).closest('.thumbnail').remove();
        $('#gallery-photo-add' + ($(this).attr('data-id') - 1)).remove();
        sum--;

    });

    // Event when click choose upload
    $(document).on('click', '.btn-primary-customized', function () {
        if (sum < 3) {
            $('#alert').css('display', 'block');
        } else {
            $('#upload').click();
        }

    });

    // Event when click choose upload
    $(document).on('click', '#img', function () {
        $('#message').remove();
        $('#gallery-photo-add' + id).click();
        $('#gallery-photo-add' + id).on('change', function () {
            imagesPreview(this, 'div.gallery');
        });
    });
});
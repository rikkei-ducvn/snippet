<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Upload image</title>
    <link type="text/css" rel="stylesheet" href="asset/css/bootstrap.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="asset/css/main.css"/>
    <script src="asset/js/jquery.min.js"></script>
    <script src="asset/js/index.js?cache=<?php echo rand(); ?>"></script>
</head>
<body>
<div>
    <?php
    require('header.php');
    ?>
</div>
<div class="container">
    <div style="margin-top:55px;">
        <div class="col-xs-12" id="alert" style="display: none;">
            <div class="alert alert-danger">
                <ul>
                    <li>Bạn cần đăng ít nhất 3 ảnh</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 _1leuGpQP7fsY3XCp-3eNDZ">
            <div class="gallery">
                <div class="_3wLdyQ1JXeAenU3yteGpQI">
                    <button type="button" id="img" class="btn btn-brand btn-large"><i class="fa fa-camera fa-4x"></i><i>+</i>
                    </button>
                </div>
            </div>
        </div>
        <form action="index.php" method="POST" enctype="multipart/form-data">
            <input type="file" name="gallery-photo-add[]" id="gallery-photo-add0"
                   style="visibility: hidden; height: 0;"/>
            <button type="submit" name="upload" id="upload" class="btn btn-primary"
                    style="display: none; margin-left: 20px;">Upload
            </button>
        </form>
    </div>
    <?php
    // Upload file
    if (isset($_POST['upload'])) {
        if (isset($_FILES['gallery-photo-add'])) {
            try {
                $fileCount = count($_FILES['gallery-photo-add']['name']);
                $error = false;
                if ($fileCount > 2) {
                    for ($i = 0; $i < $fileCount - 1; $i++) {
                        $fileName = $_FILES['gallery-photo-add']['name'][$i];
                        $allowed = array('gif', 'png', 'jpg', 'jpeg', 'GIF', 'PNG', 'JPG', 'JEPG');
                        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                        $rawBaseName = pathinfo($fileName, PATHINFO_FILENAME);

                        // Check extension
                        if (in_array($ext, $allowed)) {
                            if (!is_dir('upload')) {
                                mkdir('upload');
                            }

                            $counter = 0;

                            // Rename file exist
                            while (file_exists('upload/' . $fileName)) {
                                $fileName = $rawBaseName . $counter . '.' . $ext;
                                $counter++;
                            };

                            move_uploaded_file($_FILES['gallery-photo-add']['tmp_name'][$i], 'upload/' . $fileName);
                        } else {
                            echo '<div class="alert alert-warning">' . $fileName . ' is not a image and it has not been upload</div></br>';
                        }
                    }
                    echo '<div class="alert alert-success">Complete Upload image</div></br>';
                } else {
                    echo '<div id="message">Wrong number image</div></br>';
                }
            } catch (Exception $e) {
                echo '<div class="alert alert-danger">Upload Fail</div></br>';
            }
        }
    }
    ?>
    <div>
        <?php
        require('footer.php');
        ?>
    </div>
</div>
</body>
</html>

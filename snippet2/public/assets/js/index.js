// Get detail area
$(document).on('click', '.edit', function () {

    // Load form data
    $.ajax({
        url: '/snippet/snippet2/analytics/search/console/detail',
        method: 'GET',
        dataType: 'json',
        async: false,
        data: {
            id: $(this).attr('data-id')
        },
        success: function (res) {
            if (res.err) {
                alert(res.msg);
            } else {
                $('#editEmployeeModal').modal('show');
                $('#id').val(res.data.id);
                $('#date').val(res.data.date);
                $('#keyword').val(res.data.keyword);
                $('#click_number').val(res.data.click_number);
                $('#display_times').val(res.data.display_times);
                $('#ctr').val(res.data.ctr);
                $('#posting_rank').val(res.data.posting_rank);

                // Hidden data to check
                $('#date_check').val(res.data.date);
                $('#keyword_check').val(res.data.keyword);
                $('#click_number_check').val(res.data.click_number);
                $('#display_times_check').val(res.data.display_times);
                $('#ctr_check').val(res.data.ctr);
                $('#posting_rank_check').val(res.data.posting_rank);
            }
        },
        error: function (res) {
            alert(res.msg);
        }
    });
});

// Update area
$(document).on('click', '.save', function () {
    // Load form data
    $.ajax({
        url: '/snippet/snippet2/analytics/search/console/update',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: $('#form-update').serializeArray(),
        success: function (res) {
            $('#editEmployeeModal').modal('hide');
            alert(res.msg);
            location.reload();
        },
        error: function (err) {
            $('#editEmployeeModal').modal('hide');
            alert(err.statusText);
            location.reload();
        }
    });
});
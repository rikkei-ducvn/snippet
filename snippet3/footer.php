<div class="col-xs-12">
    <div class="alert alert-info"><p><b>Để bán nhanh hơn:</b></p>
        <ul>
            <li>Chụp hình khổ ngang: phòng ngủ, phòng khách, bếp, ban công</li>
        </ul>
        <br>
        <p><b>Tin sẽ bị từ chối nếu:</b></p>
        <ul>
            <li>Sử dụng hình ảnh trùng lặp hoặc lấy từ Internet</li>
            <li>Chèn số điện thoại/email/logo vào hình</li>
        </ul>
    </div>
</div>
<div class="footer navbar-fixed-bottom formFooter _29qtwZnBfU4ggKBsMxnCL0">
    <div class="container">
        <div class="btn-group btn-group-justified"><a class="btn btn-lg btn-primary btn-primary-customized"
                                                      type="submit"><span
                        style="vertical-align: middle;">TIẾP TỤC</span><!-- react-text: 1073 --> <!-- /react-text -->
            </a></div>
    </div>
</div>
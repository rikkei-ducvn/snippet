<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php echo Asset::css('bootstrap.css') ?>
    <!-- Javascript -->
    <?php echo Asset::js('jquery-3.1.1.min.js') ?>
    <?php echo Asset::js('bootstrap.js') ?>
    <?php echo Asset::js('index.js') ?>
    
</head>
<body>
<div id="wrapper">
    <!--contents-->
    <?php echo $content; ?>
</div>
</body>
</html>

<?php

/**
 * The Controller_Analytics_Search_Console
 */
class Controller_Analytics_Search_Console extends Controller_Base
{
    /**
     * @var Business_Analytics_Search_Console
     */
    private $analytics_search_console_business;

    /**
     * Constructor.
     *
     * @access public
     * @param \Fuel\Core\Request $request
     */
    public function __construct(\Fuel\Core\Request $request)
    {
        parent::__construct($request);
        $this->analytics_search_console_business = new Business_Analytics_Search_Console();
    }

    /**
     * The Index page
     * Show the view of master console
     */
    public function action_index()
    {
        $consoles = $this->analytics_search_console_business->search_consoles();

        $title = 'Snippet2';
        $view = View::forge('analytics/search/index', compact('title', 'consoles'));
        $this->template->title = $title;
        $this->template->content = $view;
    }

    /**
     * The Insert page
     * Insert 12000 random data
     */
    public function post_insert()
    {
        try {
            DB::start_transaction();
            $this->analytics_search_console_business->insert_console();
            DB::commit_transaction();
            Response::redirect('analytics/search/console');
        } catch (Exception $e) {
            DB::rollback_transaction();
            $title = 'Error';
            $this->template->title = $title;
            $this->template->content = $e->getMessage();
        }
    }

    /**
     * The detail page
     *
     * @return string json data
     */
    public function action_detail()
    {
        try {
            // Get console
            $id = Input::get('id');
            $data = $this->analytics_search_console_business->get_detail($id);
            if (!$data) {
                return $this->response(array(
                    'err' => true,
                    'msg' => 'Data not found'
                ));
            }

            return $this->response(array(
                'err' => false,
                'data' => $data
            ));
        } catch (\Exception $e) {
            return $this->response(array(
                'err' => true,
                'msg' => $e->getMessage()
            ));
        }
    }

    /**
     * Action post update
     * @return mixed
     */
    public function post_update()
    {
        try {
            // Check console exist
            $id = Input::post('id');
            $console = $this->analytics_search_console_business->get_detail($id);

            if (!$console) {
                return $this->response(array(
                    'msg' => 'Data not found'
                ));
            }

            // Check had not been update by another member
            if (
                $console['date'] != Input::post('date_check') ||
                $console['keyword'] != Input::post('keyword_check') ||
                $console['click_number'] != Input::post('click_number_check') ||
                $console['display_times'] != Input::post('display_times_check') ||
                $console['ctr'] != Input::post('ctr_check') ||
                $console['posting_rank'] != Input::post('posting_rank_check')
            ) {
                return $this->response(array(
                    'msg' => 'Data had not been update by another member'
                ));
            }

            $this->analytics_search_console_business->update_console($id);

            return $this->response(array(
                'msg' => 'SUCCESSFUL  UPDATE DB'
            ));
        } catch (Exception $e) {
            return $this->response(array(
                'msg' => $e->getMessage()
            ));

        }
    }
}

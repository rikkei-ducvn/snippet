<?php

/**
 * Class Controller_Base
 */
class Controller_Base extends \Fuel\Core\Controller_Template
{
    /**
	* @var string page template
	*/
    public $template = 'template/main';

    /**
     * Response
     *
     * @access protected
     * @param array $data
     * @return string json data
     */
    protected function response($data)
    {
        echo Format::forge($data)->to_json();
        exit;
    }
}
